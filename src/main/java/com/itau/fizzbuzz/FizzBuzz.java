package com.itau.fizzbuzz;

public class FizzBuzz {

	public static String contarFizzBuzz(int numero) {
		
		StringBuilder builder = new StringBuilder();
		String str = " ";
		
		for (int i = 1; i <= numero; i++) {
			
			builder.append(fizzBuzz(i) + str);
		}
		
		return builder.toString();
	}
	
	public static String fizzBuzz(int i) {
//		if (i % 3 == 0) {
//			return "fizz";
//		}
//		return null;
		if ((i % 3 ==0) && (i % 5 ==0))  {
			return "fizzbuzz";
		}
		
		if (i % 5 == 0) {
			return "buzz";
		}
		
		if ( i % 3 == 0) {
			return "fizz";
			
		}

		return Integer.toString(i);
		
		
	}

	
}
